<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Web del TEU</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

</head>
        
<body>
	
	<nav style = "padding: .5em 1em" class="nav">
			<a class="navbar-brand" href="{{route('home')}}"><img src="../static_img/logo.jpg" alt="Logo del TEU" width="100" height="50"></a>
			<a class="nav-link" href="{{route('obras.filter', null)}}">Hacer una consulta</a>
			<a class="nav-link" href="{{route('galeria.index')}}">Galería de imágenes</a>
			@if(!Auth::check())
				<a class="nav-link" href="/login">Iniciar sesión <span class="sr-only">(current)</span></a>
			@else
				<a class="nav-link" href="{{route('obras.create')}}">Insertar datos <span class="sr-only">(current)</span></a>
				<a class="nav-link" href="/logout">Cerrar Sesión de {{Auth::user()->name}}</a>
			@endif
		</nav>

@yield('contenido')
</body>
<footer style="margin:0;">
	<div class="container" style="margin: 0; padding: 0; font-size: 9px">
		<img src="../static_img/ministerio.png" alt="Ministerio de Economía, Comercio y Competitividad; Secretaría de Estado de Investigación, Desarrollo e Innovación">
		<b>Proyecto de Investigación HISTORIA DEL TEATRO UNIVERSITARIO ESPAÑOL: FFI2015-66393-P</b>
	</div>
</footer>
<script src="/js/all.js"></script>
</html>
