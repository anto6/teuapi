@extends('layout_def')

@section('contenido')

<div class="row" style="padding: 3em;">
	@foreach($response as $img)
		<div class="col-md-4">
    		<div class="card">
  				<img class="card-img-top" src="{{Storage::url($img->ruta)}}" alt="" style="width:100%; max-height: 400px;">
  				<div class="card-footer">
  				<br>
  				<br>
    			<a href="{{route('obras.show', $img->obra_id)}}" class="btn btn-primary">Accede a la ficha</a>
  			</div>
		</div>
				
			</div>
	@endforeach
</div>
@stop