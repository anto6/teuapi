@extends('layout_def')

@section('contenido')

<!-- Form contact -->
<br>
<div class="container">
<form method="GET" action="{{route('obras.filter')}}">
 {!! csrf_field() !!}
    <p class="h2 text-center mb-4">Realice una consulta en la base de datos</p>

    <div class="md-form">
    	<label>Título de la obra</label>
        <input type="text" name="titulo" class="form-control">
    </div>
	<br>
    <div class="md-form">
    	<label>Autor de la obra</label>
        <input type="text" name="autoria" class="form-control">
    </div>
	<br>
    <div class="md-form">
    	<label>Género dramático</label>
        <input type="text" name="genero_dramatico" class="form-control">
    </div>
    <br>
    <div class="md-form">
    	<label>Traducción</label>
        <input type="text" name="traduccion" class="form-control">
    </div>
    <br>
    <div class="md-form">
    	<label>Adaptacion</label>
        <input type="text" name="adaptacion" class="form-control">
    </div>
	
	<br>
    <div class="md-form">
    	<label>Dirección</label>
        <textarea rows="4" type="text" name="direccion" class="form-control"></textarea>
    </div>
	<br>
    <div class="md-form">
    	<label>Reparto</label>
        <textarea rows="13" type="text" name="reparto" class="form-control"></textarea>
    </div>
    <br>
    <div class="md-form">
    	<label>Escenografía</label>
        <textarea rows="5" type="text" name="escenografia" class="form-control"></textarea>
    </div>
    <br>
    <div class="md-form">
        <label>Figurines</label>
        <textarea rows="5" type="text" name="figurines" class="form-control"></textarea>
    </div>
    <br>
    <div class="md-form">
        <label>Sonido y Música</label>
        <textarea rows="5" type="text" name="sonido_y_musica" class="form-control"></textarea>
    </div>
    <br>
     <div class="md-form">
        <label>Coreografía</label>
        <textarea rows="5" type="text" name="coreografia" class="form-control"></textarea>
    </div>
    <br>
    <div class="md-form">
        <label>Fecha de estreno</label>
        <input rows="5" type="date" name="fecha_de_estreno" class="form-control"></textarea>
    </div>
    <br>
     <div class="md-form">
        <label>Reseñas críticas</label>
        <textarea rows="5" type="text" name="resenyas_criticas" class="form-control"></textarea>
    </div>
    <br>
     <div class="md-form">
        <label>Material Gráfico</label>
        <textarea rows="5" type="text" name="material_grafico" class="form-control"></textarea>
    </div>
    <br>
        <div class="text-center">
        <button class="btn btn-primary">BUSCAR<i class="fa fa-paper-plane-o ml-1"></i></button>
    <br>
    <br>

    </div>
</form>
<!-- Form contact -->
            

@stop