@extends('layout_def')

@section('contenido')

<div class="container">
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">{{$obra[0]->titulo}}</h1>
    @if ($obra[0]->usr_creador == $obra[0]->usr_last_mod)
    	<p class="lead">Ficha realizada por: {{$obra[0]->usr_creador}}</p>
    @else
    	<p class="lead">Ficha realizada por: {{$obra[0]->usr_creador}}</p>
    	<p class="lead">Última modificación: {{$obra[0]->usr_last_mod}}</p>
    @endif
	
	<br>
	@if(Auth::check())
	<div class="row">
		<div class="col-md-3">
	    <a class="btn btn-primary" href="{{route('obras.edit', $obra[0]->id)}}">Editar entrada</a>
	    </div>
	    <div class="col-md-3">
		<form action="{{route('obras.destroy', $obra[0]->id)}}" method="POST">
			{!! csrf_field() !!}
	 		{!!method_field('DELETE')!!}
			<input type="submit" class="btn btn-danger" value="Borrar entrada">
		</form>
		</div>
	</div>
	@endif
  </div>
</div>
	
<div class="container">
	<div class="row">
		<div class="col-md-6">
			@if($obra[0]->autoria != null)
				<h3>Autor:</h3> <p class="lead">{{$obra[0]->autoria}}</p>
			@endif
			@if($obra[0]->genero_dramatico != null)
				<h3>Género dramático:</h3> <p class="lead">{{$obra[0]->genero_dramatico}}</p>
			@endif
			@if($obra[0]->traduccion != null)
				<h3>Traducción: </h3>  <p class="lead">{{$obra[0]->traduccion}}</p>
			@endif
			@if($obra[0]->adaptacion != null)
				<h3 >Adaptación: </h3> <p class="lead">{{$obra[0]->adaptacion}}</p>
			@endif
			@if($obra[0]->direccion != null)
				<h3 >Dirección: </h3>
				@foreach($obra[0]->direccion as $valor)
					<p class="lead">{{$valor}}</p>
				@endforeach
			@endif
			@if($obra[0]->reparto != null)
				<h3>Reparto:</h3>
				@foreach($obra[0]->reparto as $valor)
					<p class="lead">{{$valor}}</p>
				@endforeach
			@endif
			@if($obra[0]->escenografia != null)
			<h3>Escenografía: </h3>
				@foreach($obra[0]->escenografia as $valor)
					<p class="lead">{{$valor}}</p>
				@endforeach
			@endif

			@if($obra[0]->figurines != null)
				<h3 >Figurines: </h3>
				@foreach($obra[0]->figurines as $valor)
					<p class="lead">{{$valor}}</p>
				@endforeach
			@endif

			@if($obra[0]->sonido_y_musica != null)
				<h3 >Sonido y Música: </h3>
				@foreach($obra[0]->sonido_y_musica as $valor)
					<p class="lead">{{$valor}}</p>
				@endforeach
			@endif
		</div>
		<div class="col-md-6">
			@if($obra[0]->coreografia != null)
				<h3 >Coreografía: </h3>
				@foreach($obra[0]->coreografia as $valor)
					<p class="lead">{{$valor}}</p>
				@endforeach
			@endif
			@if($obra[0]->otros != null)
				<h3 >Otros: </h3>
				@foreach($obra[0]->otros as $valor)
					<p class="lead">{{$valor}}</p>
				@endforeach
			@endif
			@if($obra[0]->fecha_de_estreno != null)
				<h3>Fecha de estreno:</h3>
				<p class="lead">{{$obra[0]->fecha_de_estreno}}</p>
			@endif
			@if($obra[0]->resenyas_criticas != null)
				<h3 >Reseñas Críticas: </h3>
				@foreach($obra[0]->resenyas_criticas as $valor)
					<p class="lead">{{$valor}}</p>
				@endforeach
			@endif
			@if($obra[0]->bibliografia != null)
				<h3 >Bibliografía: </h3>
				@foreach($obra[0]->bibliografia as $valor)
					<p class="lead">{{$valor}}</p>
				@endforeach
			@endif
			@if($obra[0]->material_grafico != null)
				<h3 >Material gráfico: </h3>
				@foreach($obra[0]->material_grafico as $valor)
					<p class="lead">{{$valor}}</p>
				@endforeach
			@endif
			@if($obra[0]->comentarios != null)
				<h3 >Comentarios: </h3>
				@foreach($obra[0]->comentarios as $valor)
					<p class="lead">{{$valor}}</p>
				@endforeach
			@endif
		</div>
	</div>
	
	<div style="padding-bottom: .5em">
		@if(!$obra[0]->foto->isempty())
			<h3>Imágenes: </h3>
			<div class="row">
				@foreach($obra[0]->foto as $img)
					<div class="col-md-6">
						<a href="{{Storage::url($img->ruta)}}">
			  				<img class="img-responsive" src="../{{Storage::url($img->ruta)}}" alt="" style="width:100%; max-height: 400px;">
			  			</a>
					</div>
				@endforeach
			</div>
		@endif
	</div>

	<div style="padding-bottom: .5em">
		@if(!$obra[0]->fichero->isempty())
			<h3>Ficheros relacionados</h3>
			<ul class="list-group">
	  			@foreach($obra[0]->fichero as $fichero)
					<li class="list-group-item">
						<div class="col-md-10">
							{{$fichero->nombre . ".pdf"}}
						</div>
						<div class="col-md-2"> 
							<a class="btn btn-primary" href="{{Storage::url($fichero->ruta)}}" style="width:100%;">Ver</a>
						</div>
					</li>
				@endforeach
			</ul>
		@endif
	</div>

</div>
</div>

@stop