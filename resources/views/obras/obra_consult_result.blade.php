@extends('layout_def')

@section('contenido')

<div class="container" style="margin: 1em 3em 0 3em;">
<h2>Consulta</h2>
<br>
<div class="col-md-6">
	<form class="navbar-form navbar-left" role="search" action="{{route('obras.quicksearch')}}" method="GET">
		<div class="form-group">
			<div class="row">
				<div class="col-md-6" style="padding: 0">
					<input type="text" name="titulo" class="form-control" placeholder="Búsqueda rápida">
				</div>
				<div class="col-md-4" style="padding: 0">
				<input type="submit" class="btn btn-default" value="Buscar">
				</div>
			</div>
		</div>
	</form>
</div>
<div class="col-md-4" style="padding: 0">
	<a href={{route('search')}} class="btn btn-primary"> Búsqueda avanzada</a>
</div>
<br>
<br>
<h2>Resultados:</h2>
<br>
<div class="row">
	@foreach($resultado as $valor)
		<div class="card col-lg-5" style="margin: .5em; padding: 0;">
			<div class="card-header">
				<h3>{{$valor->titulo}}</h3>
			</div>
			<div class="card-body">
				<ul>
					<li>
						Autor: {{$valor->autoria}}
					</li>
					<li>
						Creado por: {{$valor->usr_creador}}
					</li>
				</ul>
			</div>
			<div class="card-footer">
				<a class="btn btn-primary" href="{{route('obras.show', $valor->id)}}">Ver ficha completa</a>
			</div>
		</div>
	@endforeach
</div>
</div>

@stop