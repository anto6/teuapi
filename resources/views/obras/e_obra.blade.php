@extends('layout_def')

@section('contenido')

<!-- Form contact -->
<br>
<div class="container">
<form method="POST" action="{{route('obras.update', $obra->id)}}" enctype="multipart/form-data">
 {!! csrf_field() !!}
 {!!method_field('PUT')!!}
    <p class="h2 text-center mb-4">Modifique los datos de la obra que considere necesarios</p>

    <div class="md-form">
    	<label>Título de la obra</label>
        <input type="text" name="titulo" class="form-control" maxlength="250" value="{{$obra->titulo}}">
    </div>
	<br>
    <div class="md-form">
    	<label>Autor de la obra</label>
        <input type="text" name="autoria" class="form-control" maxlength="250" value="{{$obra->autoria}}">
    </div>
	<br>
    <div class="md-form">
    	<label>Género dramático</label>
        <input type="text" name="genero_dramatico" class="form-control" maxlength="250" value="{{$obra->genero_dramatico}}">
    </div>
	<br>
    <div class="md-form">
    	<label>Traducción</label>
        <input type="text" name="traduccion" class="form-control" maxlength="250" value="{{$obra->traduccion}}">
    </div>
    <br>
    <div class="md-form">
    	<label>Adaptacion</label>
        <input type="text" name="adaptacion" class="form-control" maxlength="250" value="{{$obra->adaptacion}}">
    </div>
	<br>
    <div class="md-form">
    	<label>Dirección</label>
        <textarea rows="4" type="text" name="direccion" class="form-control">{{$obra->direccion}}</textarea>
    </div>
	<br>
    <div class="md-form">
    	<label>Reparto</label>
        <textarea rows="13" type="text" name="reparto" class="form-control">{{$obra->reparto}}</textarea>
    </div>
    <br>
    <div class="md-form">
    	<label>Escenografía</label>
        <textarea rows="5" type="text" name="escenografia" class="form-control">{{$obra->escenografia}}</textarea>
    </div>
    <br>
    <div class="md-form">
        <label>Figurines</label>
        <textarea rows="5" type="text" name="figurines" class="form-control">{{$obra->figurines}}</textarea>
    </div>
    <br>
    <div class="md-form">
        <label>Sonido y Música</label>
        <textarea rows="5" type="text" name="sonido_y_musica" class="form-control">{{$obra->sonido_y_musica}}</textarea>
    </div>
    <br>
     <div class="md-form">
        <label>Coreografía</label>
        <textarea rows="5" type="text" name="coreografia" class="form-control">{{$obra->coreografia}}</textarea>
    </div>
    <br>
     <div class="md-form">
        <label>Otros</label>
        <textarea rows="5" type="text" name="otros" class="form-control">{{$obra->otros}}</textarea>
    </div>
    <br>
     <div class="md-form">
        <label>Fecha de estreno</label>
        <input rows="5" type="date" name="fecha_de_estreno" class="form-control" value="{{$obra->fecha_de_estreno}}"></textarea>
    </div>
    <br>
     <div class="md-form">
        <label>Reseñas críticas</label>
        <textarea rows="5" type="text" name="resenyas_criticas" class="form-control">{{$obra->resenyas_criticas}}</textarea>
    </div>
    <br>
     <div class="md-form">
        <label>Bibliografía</label>
        <textarea rows="5" type="text" name="bibliografia" class="form-control">{{$obra->bibliografia}}</textarea>
    </div>
    <br>
     <div class="md-form">
        <label>Material Gráfico</label>
        <textarea rows="5" type="text" name="material_grafico" class="form-control">{{$obra->material_grafico}}</textarea>
    </div>
    <br>
    <div class="md-form">
        <label>Comentarios</label>
        <textarea rows="5" type="text" name="comentarios" class="form-control">{{$obra->comentarios}}</textarea>
        <input type="hidden" name="usr_creador" value={{$obra->usr_creador}}>
         <input type="hidden" name="usr_last_mod" value={{Auth::user()->name}}>
    </div>
    <div class="md-form">
        <label>Añadir más imágenes</label>
        <input type="file" name="img[]" multiple>
    </div>
    <br>
    <div class="md-form">
        <label>Añadir más ficheros</label>
        <input type="file" name="fich[]" multiple>
    </div>
    <br>
        <div class="text-center">
        <button class="btn btn-primary">Modificar<i class="fa fa-paper-plane-o ml-1"></i></button>
    <br>
    <br>

    </div>
</form>
<!-- Form contact -->
            

@stop