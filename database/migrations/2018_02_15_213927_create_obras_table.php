<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('obras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('autoria')->nullable($value = true);
            $table->string('genero_dramatico')->nullable($value = true);
            $table->string('fecha_de_estreno')->nullable($value = true);
            $table->string('traduccion')->nullable($value = true);
            $table->string('adaptacion')->nullable($value = true);
            $table->text('direccion')->nullable($value = true);
            $table->text('reparto')->nullable($value = true);
            $table->text('escenografia')->nullable($value = true);
            $table->text('figurines')->nullable($value = true);
            $table->text('sonido_y_musica')->nullable($value = true);
            $table->text('coreografia')->nullable($value = true);
            $table->text('otros')->nullable($value = true);
            $table->text('resenyas_criticas')->nullable($value = true);
            $table->text('bibliografia')->nullable($value = true);
            $table->text('material_grafico')->nullable($value = true);
            $table->text('comentarios')->nullable($value = true);
            $table->string('usr_creador')->nullable($value = true);
            $table->string('usr_last_mod')->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('obras');
    }
}
