<?php

namespace App\Http\Controllers;

use Storage;

use App\Obra;

use App\Foto;

use Auth;

use App\Fichero;

use Illuminate\Http\Request;

class ObrasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('obras/c_obra');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Obra::create($request->all())->id;
        if($request->has('img'))
        {
            foreach($request->img as $foto)
            {
                Foto::create(['obra_id'=>$id, 'ruta' => $foto->store('public/img')]);
            }
        }
        if($request->has('fich'))
        {
            foreach($request->fich as $fichero)
            {
                Fichero::create(['obra_id'=>$id,'nombre'=>pathinfo($fichero->getClientOriginalName(), PATHINFO_FILENAME), 'ruta'=>$fichero->store('public/ficheros')]);
            }
        }

        return redirect()->route('obras.show', $id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obra = Obra::with('foto', 'fichero')->where('id', $id)->get();
        if($obra[0]->direccion != null)
            $obra[0]->direccion = explode(";", $obra[0]->direccion);
        if($obra[0]->reparto != null)
            $obra[0]->reparto = explode(";", $obra[0]->reparto);
        if($obra[0]->escenografia != null)
            $obra[0]->escenografia = explode(";", $obra[0]->escenografia);
        if($obra[0]->figurines != null)
            $obra[0]->figurines = explode(";", $obra[0]->figurines);
        if($obra[0]->sonido_y_musica != null)
            $obra[0]->sonido_y_musica = explode(";", $obra[0]->sonido_y_musica);
        if($obra[0]->coreografia != null)
            $obra[0]->coreografia = explode(";", $obra[0]->coreografia);
        if($obra[0]->otros != null)
            $obra[0]->otros = explode(";", $obra[0]->otros);
        if($obra[0]->resenyas_criticas != null)
            $obra[0]->resenyas_criticas = explode(";", $obra[0]->resenyas_criticas);
        if($obra[0]->bibliografia != null)
            $obra[0]->bibliografia = explode(";", $obra[0]->bibliografia);
        if($obra[0]->material_grafico != null)
            $obra[0]->material_grafico = explode(";", $obra[0]->material_grafico);
        if($obra[0]->comentarios != null)
            $obra[0]->comentarios = explode(";", $obra[0]->comentarios);   
        return view( 'obras/obra_show', compact('obra'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::check())
        {
            $obra = Obra::findOrFail($id);
            return view('obras/e_obra', compact('obra'));
        }

        return redirect('/login');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $obraModificar = Obra::findOrFail($id);
            $obraModificar->update($request->all());

            if($request->has('img'))
            {
                foreach($request->img as $foto)
                {
                    Foto::create(['obra_id'=>$id, 'ruta'=>$foto->store('public/img')]);
                }
            }

            if($request->has('fich'))
            {
                foreach($request->fich as $fichero)
                {
                    Fichero::create(['obra_id'=>$id,'nombre'=>pathinfo($fichero->getClientOriginalName(), PATHINFO_FILENAME), 'ruta'=>$fichero->store('public/ficheros')]);
                }
            }

            return redirect()->route('obras.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::check())
        {
            $obraBorrar = Obra::findOrFail($id);
            $ficherosDestruir = Obra::with('foto', 'fichero')->where('id', $id)->get();

            foreach($ficherosDestruir[0]->foto as $fotos)
            {
                Storage::delete($fotos->ruta);
                $fotos->delete();
            }

            foreach($ficherosDestruir[0]->fichero as $ficheros)
            {
                Storage::delete($ficheros->ruta);
                $ficheros->delete();
            }

            $obraBorrar->delete();

            return redirect()->route('home');
        }

        return redirect('/login');
    }

    public function search()
    {
        return view('obras/obra_consult');
    }

    public function quickSearch(Request $request)
    {
        $obra = new Obra;
        $consulta = $obra->newQuery();
        $consulta->where('titulo', 'like' , '%'.$request->input('titulo').'%');
        $resultado = $consulta->get();
        return view('obras/obra_consult_result', compact('resultado'));
    }

    public function filter(Request $request)
    {
        //Creamos la nueva consulta sobre la base de datos
        $obra = new Obra;
        $consulta = $obra->newQuery();

        if($request == null)
        {
            $resultado = $consulta->get()->all();
        }
        else
        {
            if(!is_null($request->input('titulo')))
            {
                $consulta->where('titulo', 'like' , '%'.$request->input('titulo').'%');
            }
            
            if(!is_null($request->input('autoria')))
            {
                $consulta->where('autoria', 'like' , '%'.$request->input('autoria').'%');
            }
            
            if(!is_null($request->input('genero_dramatico')))
            {
                $consulta->where('genero_dramatico', 'like' , '%'.$request->input('genero_dramatico').'%');
            }

            if(!is_null($request->input('traduccion')))
            {
                $consulta->where('traduccion', 'like' , '%'.$request->input('traduccion').'%');
            }

            if(!is_null($request->input('adaptacion')))
            {
                $consulta->where('adaptacion', 'like' , '%'.$request->input('adaptacion').'%');
            }

            if(!is_null($request->input('direccion')))
            {
                $consulta->where('direccion', 'like' , '%'.$request->input('direccion').'%');
            }

            if(!is_null($request->input('reparto')))
            {
                $consulta->where('reparto', 'like' , '%'.$request->input('reparto').'%');
            }

            if(!is_null($request->input('escenografia')))
            {
                $consulta->where('escenografia', 'like' , '%'.$request->input('escenografia').'%');
            }

            if(!is_null($request->input('figurines')))
            {
                $consulta->where('figurines', 'like' , '%'.$request->input('figurines').'%');
            }

            if(!is_null($request->input('sonido_y_musica')))
            {
                $consulta->where('sonido_y_musica', 'like' , '%'.$request->input('sonido_y_musica').'%');
            }

            if(!is_null($request->input('coreografia')))
            {
                $consulta->where('coreografia', 'like' , '%'.$request->input('coreografia').'%');
            }

            if(!is_null($request->input('figurines')))
            {
                $consulta->where('figurines', 'like' , '%'.$request->input('figurines').'%');
            }

            if(!is_null($request->input('sonido_y_musica')))
            {
                $consulta->where('sonido_y_musica', 'like' , '%'.$request->input('sonido_y_musica').'%');
            }

            if(!is_null($request->input('coreografia')))
            {
                $consulta->where('coreografia', 'like' , '%'.$request->input('coreografia').'%');
            }

            if(!is_null($request->input('resenyas_criticas')))
            {
                $consulta->where('material_grafico', 'like' , '%'.$request->input('material_grafico').'%');
            }

            if(!is_null($request->input('fecha_de_estreno')))
            {
                $consulta->where('fecha_de_estreno', $request->input('fecha_de_estreno'));
            }

            $resultado = $consulta->get();
        }

        return view('obras/obra_consult_result', compact('resultado'));
    }
}
