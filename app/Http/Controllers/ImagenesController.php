<?php

namespace App\Http\Controllers;

use App\Foto;

use Illuminate\Http\Request;

class ImagenesController extends Controller
{
    public function index()
    {
    	$response = Foto::all();
    	return view('galeria/galeria', compact('response'));
    }
}
