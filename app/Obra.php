<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Obra extends Model
{
     protected $fillable = ['id', 'titulo', 'autoria', 'genero_dramatico', 'fecha_de_estreno', 'traduccion', 'adaptacion', 'direccion', 'reparto', 'escenografia', 'figurines', 'sonido_y_musica', 'coreografia', 'otros', 'resenyas_criticas', 'bibliografia', 'material_grafico', 'comentarios', 'usr_creador', 'usr_last_mod'];

     public function foto()
     {
     	return $this->hasMany(Foto::class);
     }

     public function fichero()
     {
     	return $this->hasMany(Fichero::class);
     }
}
