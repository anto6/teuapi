<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fichero extends Model
{
    protected $fillable = ['nombre', 'obra_id', 'ruta', 'created_at', 'updated_at'];
    
    public function obra()
    {
    	return $this->belongsTo(Obra::class);
    }
}
