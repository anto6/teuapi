<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
	protected $fillable = ['obra_id', 'ruta', 'created_at', 'updated_at'];
    
    public function obra()
    {
    	return $this->belongsTo(Obra::class);
    }
}
