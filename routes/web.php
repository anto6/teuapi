<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('creausuario', function(){

	$usuario = new App\User;
	$usuario->name = "Administrador";
	$usuario->email = "administrador@correocorrecto.com";
	$usuario->password = bcrypt('admin');
	$usuario->save();

	return $usuario;

});

Route::resource('obras', 'ObrasController');

Route::get('/', ['as' => 'home', 'uses' => 'PagesController@home']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('obra/search', ['as' => 'search', 'uses' => 'ObrasController@search']);

Route::get('obra/filter', ['as' => 'obras.filter', 'uses' => 'ObrasController@filter']);

Route::get('galeria/index', ['as' => 'galeria.index', 'uses' => 'ImagenesController@index']);

Route::get('obra/quick', ['as' => 'obras.quicksearch', 'uses' => 'ObrasController@quickSearch']);

Auth::routes();

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
