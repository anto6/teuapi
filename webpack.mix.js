let mix = require('laravel-mix');


mix.js(['resources/assets/js/app.js', 'node_modules/bootstrap-sass/assets/javascripts/bootstrap.js'], 'public/js/all.js')
   .sass('resources/assets/sass/app.scss', 'public/css/app.css');


mix.browserSync({

	proxy: 'teuapidb.test'
})